import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import lex
import lexlegacy
from sqlalchemy.orm import with_polymorphic

olditems = lexlegacy.session.query(with_polymorphic(lexlegacy.RootLexicon, "*")).all()

session = lex.sessionmaker()

# Pass 1: create base entries
for item in olditems:
    # Create a row in the new db, by picking the class that has the same name
    new = vars(lex)[item.__class__.__name__]()

    # Copy all fields that have the same name.
    for name in item.__class__.__dict__:
        # Check to be sure that an equivalent key exists at the *class* level
        # Also don't copy stuff starting with underscores
        if name in new.__class__.__dict__ and not name[0] == "_":
            if item.__dict__[name]:
                new.__dict__[name] = item.__dict__[name]

    # Handle "head" properly, since it changes across types
    for head in ["cy", "pr", "yiv", "otz", "py"]:
        if head in item.__dict__:
            new.head = item.__dict__[head]
            break

    # Print out what we would do
    if new.head:
        print "Will create {} entry with: ".format(new.__class__.__name__)
        for k, v in new.__dict__.items():
            if not k[0] == "_":
                if (isinstance(v, str)):
                    v = unicode(v, "utf-8", errors='ignore')
                print u"  {}: {}".format(k, v)

        session.add(new)

session.commit()

# Pass 2: create relations
for item in olditems:
    if "source" in item.__dict__ and item.source:
        rel = lex.Relation()
        rel.item_id = item.id
        rel.source_id = item.source

        # Print out what we would do
        print "Will create relationship from {} -> {}".format(rel.source_id, rel.item_id)

        session.add(rel)

session.commit()
