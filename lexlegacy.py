from sqlalchemy import Column, Integer, String, DateTime, Interval, Numeric, ForeignKey
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine

engine = create_engine('sqlite+pysqlite:///lexlegacy.db') # Note: relative to PWD
session = sessionmaker(bind=engine)()
Base = declarative_base()

class RootLexicon(Base):
    """The root class from which all tables derive"""
    __tablename__ = "RootLexicon"

    id = Column(Integer, primary_key=True)
    tableRef = Column(String, nullable=False)

    __mapper_args__ = {
        'polymorphic_identity': __tablename__,
        'polymorphic_on': tableRef
    }

class CommonYivrian(RootLexicon):
    """CommonYivrian, the ancestor of all the Yivril languages"""
    __tablename__ = "CommonYivrian"

    id = Column(Integer, ForeignKey("RootLexicon.id"), primary_key=True)
    cy = Column(String, nullable=False)
    source = Column(Integer, ForeignKey("RootLexicon.id"))
    note = Column(String)

    __mapper_args__ = {
        'polymorphic_identity': __tablename__,
        'inherit_condition': (id == RootLexicon.id)
    }

class Praseo(RootLexicon):
    """Praseo, the language of the city of Prasa"""
    __tablename__ = "Praseo"

    id = Column(Integer, ForeignKey("RootLexicon.id"), primary_key=True)
    pr = Column(String, nullable=False)
    source = Column(Integer, ForeignKey("RootLexicon.id"))
    gloss = Column(String, nullable=False)
    verb = Column(String)

    source_obj = relationship("RootLexicon", foreign_keys=source)

    __mapper_args__ = {
        'polymorphic_identity': __tablename__,
        'inherit_condition': (id == RootLexicon.id)
    }

class Yivrian(RootLexicon):
    """Praseo, the language of the city of Prasa"""
    __tablename__ = "Yivrian"

    id = Column(Integer, ForeignKey("RootLexicon.id"), primary_key=True)
    yiv = Column(String, nullable=False)
    gloss = Column(String, nullable=False)
    source = Column(Integer, ForeignKey("RootLexicon.id"))
    partOfSpeech = Column(String, nullable=False)
    nounClass = Column(String)
    verb = Column(String)
    verbForm = Column(String)
    verbClass = Column(String)
    note = Column(String)

    source_obj = relationship("RootLexicon", foreign_keys=source)

    __mapper_args__ = {
        'polymorphic_identity': __tablename__,
        'inherit_condition': (id == RootLexicon.id)
    }

class OldTzingrizhil(RootLexicon):
    """Old Tzingrizhil, the ancestor of Praseo and Tsingrizhil"""
    __tablename__ = "OldTzingrizhil"

    id = Column(Integer, ForeignKey("RootLexicon.id"), primary_key=True)
    otz = Column(String, nullable=False)
    source = Column(Integer, ForeignKey("RootLexicon.id"))

    source_obj = relationship("RootLexicon", foreign_keys=source)

    __mapper_args__ = {
        'polymorphic_identity': __tablename__,
        'inherit_condition': (id == RootLexicon.id)
    }

class ProtoYivril(RootLexicon):
    """Proto-Yivril, the ancestor of everything"""
    __tablename__ = "ProtoYivril"

    id = Column(Integer, ForeignKey("RootLexicon.id"), primary_key=True)
    py = Column(String, nullable=False)
    gloss = Column(String, nullable=False)
    note = Column(String)

    __mapper_args__ = {
        'polymorphic_identity': __tablename__,
        'inherit_condition': (id == RootLexicon.id)
    }


Base.metadata.create_all(engine)
