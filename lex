#!/usr/bin/perl
use strict;
use warnings;

use Term::Shell;
use File::Temp;
use YAML;
use Lex;
use Getopt::Long;

package main;

my $table;
my $cmd;

GetOptions( "table|t=s" => \$table,
            "cmd|c=s" => \$cmd );

my $shell = Lex::Shell->new;

if ($table) {
    $shell->cmd("use $table");
}

if ($cmd) {
    $shell->cmd($cmd);
} else {
    $shell->cmdloop;
}

exit;

package Lex::Shell;
use base qw(Term::Shell);

use Term::ANSIColor;

# The currently active table object
my $active;

sub prompt_str {
    my $str = defined $active ? "$active> " : "Lex> ";
    return colored(['green'], $str);
}

sub alias_exit {
    return "q";
}

sub err($) {
    my $errstr = shift;
    print colored['red'], $errstr, "\n";
}

sub wrn($) {
    my $wrnstr = shift;
    print colored['yellow'], $wrnstr, "\n";
}

# This sub exists to simplify argument checking for all of our shell functions.
# It takes two arguments, a condition and a message, and it prints the message
# only if the condition is false. The condition is returned to the caller.
sub require_arg($$) {
    my ($condition, $message) = @_;

    if (not $condition) {
        wrn "$message";

        my $caller = (caller 1)[3]; # get the name of the calling function
        $caller =~ s/.*run_(\w+)/$1/;

        print "Type `help $caller` for more info\n";
    }

    return $condition;
}

sub hashify($);
sub hashify($) {
    my $entry = shift;
    my $href = {};
    for my $col ($entry->columns) {
        my $v = $entry->$col;
        next if not $v;

        if (ref $v eq "Lex::RootLexicon") {
            next if not $v->id;
            $v = [ $v->tableRef, hashify $v->entry ];
        }
        $href->{$col} = $v;
    }

    YAML::Bless($href)->keys([ $entry->columns ]);
    return $href;
}

sub objectify($$);
sub objectify($$) {
    my ($href, $table) = @_;
    my ($updated, $inserted) = (0, 0);

    # objectify nested hashes
    while (my ($col, $val) = each %$href) {
        if (ref $val eq "ARRAY") {
            my ($root, $local_updated, $local_inserted);

            ($root, $local_updated, $local_inserted) = objectify $val->[1], Lex::table_class($val->[0]);
            $updated += $local_updated;
            $inserted += $local_inserted;
            ($href->{$col}) = $root->rootEntry;
        }
    }

    my $entry;
    my ($key) = $table->columns("Primary");
    if (defined $href->{$key}) {
        # modifying an existing row
        $entry = $table->retrieve($href->{$key});
        if (not $entry) {
            err "No entry matching href:\n" . stringify $href;
        } else {
            my $altered = 0;
            for my $col ($entry->columns) {
                next if not exists $href->{$col};

                # update all references and unequal columns
                no warnings 'uninitialized';
                if (ref $href->{$col}) {
                    my ($subkey) = $href->{$col}->columns("Primary");
                    if ($entry->$col->$subkey != $href->{$col}->$subkey) {
                        $entry->$col($href->{$col});
                        $altered++;
                    }
                } elsif ($entry->$col ne $href->{$col}) {
                    $entry->$col($href->{$col});
                    $altered++;
                }
            }

            if ($altered) {
                $entry->update();
                $updated++;
            }
        }
    } else {
        # inserting a new row
        $entry = $active->insert($href);
        $inserted++;
    }

    if (wantarray) {
        return $entry, $updated, $inserted;
    } else {
        return $entry;
    }
}

sub stringify(@) {
    return YAML::Dump(map { ref $_ eq 'HASH' ? $_ : hashify $_ } @_);
}

sub comp_lang($) {
    my $word = shift;
    return grep { m/^$word/ } (Lex::tables(), Lex::aliases());
}

sub comp_col($) {
    my $word = shift;
    return grep { m/^$word/ } $active->columns;
}

sub uniq(@) {
    my %uniq = map { $_ => $_ } @_;
    return sort { $a <=> $b } values %uniq;
}

sub search_term($) {
    my $term = shift;
    my $term_regex = qr/(\w+)\s*([:=])\s*(\S*)/;

    my @rs;
    if ($term eq "*") {
        # return all values
        @rs = $active->retrieve_all();
    } elsif ($term =~ m/^$term_regex$/) {
        my ($col, $op, $match) = ($1, $2, $3);
        if ($match eq "~") {
            # search for NULL fields
            @rs = $active->retrieve_from_sql("$col IS NULL");
        } elsif ($op eq ':') {
            # do a fuzzy match in a specific field
            @rs = $active->search_like($col => "%$match%");
        } elsif ($op eq '=') {
            # do an exact match in a specific field
            @rs = $active->search($col => $match);
        }
    } else {
        # look thru everything
        @rs = map { $active->search_like($_ => "%$term%") } $active->columns;
    }

    return uniq @rs;
}

sub search(@) {
    my @rs = ();
    for (@_) {
        eval { push @rs, search_term $_ };
        if ($@) {
            err "Error searching for $_:\n$@";
        }
    }
    return uniq @rs;
}

sub where(@) {
    return uniq $active->retrieve_from_sql(join " ", @_);
}

# 
# the 'use' command
#

sub run_use {
    my ($o, $lang) = @_;
    require_arg $lang, "Language name required" or return;

    my $class;
    eval { $class = Lex::table_class($lang); };
    if ($@) {
        print "Unknown language: $lang\n";
        print "Type `langs` to see available languages\n";
        return;
    }

    $active = $class;
}

sub smry_use {
    return "select the active language";
}

sub help_use {
    return
    "use <language>\n".
    "   Selects the active language, (i.e. the language used for all other commands)\n";
}

sub comp_use {
    my ($o, $word, $line, $start) = @_;
    return comp_lang $word;
}

# 
# the 'langs' command
#

sub run_langs {
    foreach my $table (Lex::tables()) {
        print "$table (", join(',', Lex::aliases($table)), ")\n";
    }
    print "\n";
}

sub smry_langs {
    return "show the available languages";
}

sub help_langs {
    return
    "langs\n".
    "   Shows the languages available in the database\n";
}

# 
# the 'desc' command
#

sub run_desc {
    my $o = shift;
    my $lang = shift() || $active;
    require_arg $lang, "Language required" or return;

    print join ", ", $lang->columns;
    print "\n";
}

sub smry_desc {
    return "show the fields for a language";
}

sub help_desc {
    return
    "desc [<language>]\n".
    "   Returns a list of the fields for the given language, or the active language if no argument is given\n";
}

sub comp_desc {
    my ($o, $word, $line, $start) = @_;
    return comp_lang $word;
}

# 
# the 'find' command
# 

sub run_find {
    my ($o, @terms) = @_;
    require_arg $active, "Active language must be set" or return;
    require_arg scalar @terms > 0, "No words to look up!" or return;

    for (search @terms) {
        eval { print stringify $_; };
        err "$_: $@" if $@;
    }
}

sub smry_find {
    return "find words in dictionaries";
}

sub help_find {
    my $msg = 
    "find <string>\n" .
    "   String may be `field:value` which searches in a specific field, or just `value`, which searches in all fields\n";
    return $msg;
}

sub comp_find {
    my ($o, $word, $line, $start) = @_;
    return comp_col $word;
}

# 
# the 'where' command
# 

sub run_where {
    my ($o, @terms) = @_;
    require_arg $active, "Active language must be set" or return;

    eval { print stringify where @terms; };
    err $@ if $@;
}

sub smry_where {
    return "run direct sql queries";
}

sub help_where {
    my $msg = 
    "where <sql>\n" .
    "   Selects entries using <sql> directly as the WHERE clause of the SELECT statement\n";
    return $msg;
}

#
# the 'edit' command
#

sub run_edit {
    my ($o, @terms) = @_;
    require_arg $active, "Active language must be set" or return;

    my @entries;
    if (scalar @terms) {
        if ($terms[0] eq "where") {
            shift @terms;
            eval { @entries = where @terms; };
            if ($@) {
                err $@;
                return;
            }
        } else {
            @entries = search @terms;
        }
        require_arg scalar @entries, "No matching entries" or return;
    } else {
        @entries = ( { map { $_ => undef } grep { not $_->groups ~~ "Primary" } $active->columns } );
    }

    # write the dump to the temp file
    my $tmp = File::Temp->new(SUFFIX => ".yaml");
    for (@entries) {
        eval{ print $tmp stringify $_; };
        err "Error with $_: $@" if $@;
    }

    # edit the temp file
    system "vi", $tmp->filename;

    # read the temp file to memory
    seek $tmp, 0, 0;
    my $read = read $tmp, my $data, -s $tmp;
    die "Couldn't read from $tmp: $!" if $read != -s $tmp;

    my ($updated, $inserted) = (0, 0);
    eval {
        for my $href (YAML::Load($data)) {
            my @data = objectify $href, $active;
            $updated += $data[1];
            $inserted += $data[2];
        }
    };
    if ($@) {
        err "$data: $@";
        return;
    }

    print "$updated edited, $inserted inserted\n";
}

sub smry_edit {
    return "edit or insert new entries into the active dictionary";
}

sub help_edit {
    my $msg = 
    "edit [<string>]\n" .
    "   Edits or inserts entries. If <string> is present, it is interpreted as a search term, and all matching entries\n" .
    " are presented for editing. If <string> is absent, and empty entry is presented for insertion.\n";
    return $msg;
}

sub comp_edit {
    my ($o, $word, $line, $start) = @_;
    return comp_col $word;
}

# 
# the 'delete' command
#

sub run_delete {
    my ($o, @terms) = @_;
    require_arg $active, "Active language must be set" or return;
    require_arg scalar @terms, "No search terms specified!" or return;

    my @entries = search @terms;
    require_arg scalar @entries, "No matching entries!" or return;

    # prompt for deletion
    my ($deleted, $unchanged) = (0, 0);
    for my $entry (@entries) {
        print stringify $entry;
        my $del = $o->prompt("Delete this entry? [y/N] ", "n", ["yes", "no"], 1);
        if ($del =~ m/^y/i) {
            eval { $entry->delete() };
            if ($@) {
                err "Unable to delete:\n$@";
                $unchanged++;
            } else {
                err "Deleted";
                $deleted++;
            }
        } else {
            print colored(['yellow'], "Not deleted\n");
            $unchanged++;
        }
    }

    print "$deleted deleted, $unchanged unchanged\n";
}

sub smry_delete {
    return "delete entries from the active dictionary";
}

sub help_delete {
    my $msg = 
    "delete <string>\n" .
    "   Deletes entries. <string> is a search term, and all matching entries are presented for deletion.\n";
    return $msg;
}

sub comp_delete {
    my ($o, $word, $line, $start) = @_;

    # offer column names
    return map { "$_:" } grep { m/^$word/ } $active->columns;
}

# 
# the 'export' command
#

sub run_export {
    my ($o, $col, $fname) = @_;
    require_arg $col, "Need column name" or return;
    require_arg $fname, "Need file name" or return;
    require_arg $active, "Active language must be set" or return;

    my @list = map { $_->$col } $active->retrieve_all();

    if (open my $fh, ">", $fname) {
        print $fh join("\n", @list);
    } else {
        err "Couldn't open $fname: $!";
    }
}

sub smry_export {
    return "export a column to a text file";
}

sub help_export {
    my $msg = 
    "export <column> <filename>\n" .
    "   Prints the contents of <column> to <filename>, one row on each line.\n";
    return $msg;
}

sub comp_export {
    my ($o, $word, $line, $start) = @_;

    my @words = split /\s/, $line;
    if (@words < 2) {
        return comp_col $word;
    }
}

# 
# the 'phonix' command
#

sub smry_phonix {
    return 'run phonix to generate or update entries from a parent language';
}

sub help_phonix {
    my $msg =
    "phonix <parent-lang> <child-lang> [<options>]\n" .
    "   Runs phonix to insert or update entries from a parent language into a child language. Options:\n" .
    "       -f      \"force\" mode; insert/update all entries without prompting\n" .
    "       -i      insert new entries for parent entries not found in the child lang\n" .
    "       -u      update entries in the child lang based on a linked entry in the parent lang\n" .
    "   The default is equivalent to -u. To only insert, specify -i; to both insert and update specify -i -u\n" .
    "   Existing entries which are not linked to a parent language entry are not modified by this command.\n";
    return $msg;
}

sub comp_phonix {
    my ($o, $word, $line, $start) = @_;
    return comp_lang $word;
}

sub run_phonix {
    my ($o, $parent, $child, @args) = @_;
    require_arg $parent, "Must specify parent language" or return;
    require_arg $child, "Must specify child language" or return;

    my ($parent_table, $child_table);
    eval { $parent_table = Lex::table_class($parent); };
    if ($@) {
        err "Unknown parent language: $parent";
        return;
    }

    eval { $child_table = Lex::table_class($child); };
    if ($@) {
        err "Unknown child language: $child";
        return;
    }

    # Handle args. If they passed -i, only insert. If they passed -u (or
    # nothing), only update. Pass both to do both.
    my $insert = scalar grep /-i/, @args;
    my $update = scalar grep /-u/, @args;
    if (not ($insert or $update)) {
        $update = 1;
    }

    # Get the lexical column for both parent and child tables. This should
    # always be the first (or only) alias.
    my $parent_alias = (Lex::aliases($parent_table->table))[0];
    require_arg $parent_alias, "Cannot run phonix on $parent: no lexical column specified in table definition" or return;
    my $child_alias = (Lex::aliases($child_table->table))[0];
    require_arg $child_alias, "Cannot run phonix on $child: no lexical column specified in table definition" or return;

    # Variables used by all commands below. Note that "in" and "out" are used
    # from the POV of phonix, so we *write* to "in" and *read* from "out".
    my $infile = "tmp.$parent_alias.lex";
    my $outfile = "tmp.$child_alias.lex";
    my $phonix_file = "$child_alias/$child_alias.phonix";
    if (not -e $phonix_file) {
        print "Could not find $phonix_file. Ensure that the directory structure is correct.\n";
        return;
    }
    
    # Handle insertions
    if ($insert) {
        my @parent_entries = $parent_table->retrieve_from_sql("id NOT IN (SELECT source FROM " . $child_table->table . ")");
        my @to_insert = invoke_phonix($phonix_file, $infile, $outfile, map { $_->$parent_alias } @parent_entries);

        # Assert that both of our arrays have the same length
        if (scalar @to_insert != @parent_entries) {
            return err "Number of entries to insert does not match number of parent entries. Going to die now.";
        }

        my $inserted = 0;
        while (@to_insert and @parent_entries) {
            my $parent_entry = shift @parent_entries;
            my $to_insert = shift @to_insert;
            chomp $to_insert;

            my $insert_href = { $child_alias => $to_insert, source => $parent_entry->id };
            foreach my $col ($child_table->_essential) {
                if (not exists $insert_href->{$col}) {
                    $insert_href->{$col} = "TODO"; # pray that there are no non-string essential columns
                }
            }

            $child_table->insert($insert_href);
            $inserted++;
        }

        print "Inserted $inserted entries\n";
    }

    # Handle updates
    if ($update) {
        my @parent_entries = $parent_table->retrieve_from_sql("id IN (SELECT source FROM " . $child_table->table . ")");
        my @to_update = invoke_phonix($phonix_file, $infile, $outfile, map { $_->$parent_alias } @parent_entries);

        if (scalar @to_update != @parent_entries) {
            return err "Number of entries to update does not match number of parent entries. Going to die now.";
        }

        my $updated = 0;
        while (@to_update and @parent_entries) {
            my $parent_entry = shift @parent_entries;
            my $to_update = shift @to_update;
            chomp $to_update;

            my @matching_entries = $child_table->search(source => $parent_entry->id);
            if (not @matching_entries) {
                wrn "No child entry found matching $parent_table->table $parent_entry->id?!";
                next;
            }

            foreach (@matching_entries) {
                $_->$child_alias($to_update);
                $_->update();;
                $updated++;
            }
        }

        print "Updated $updated entries\n";
    }
}

sub invoke_phonix {
    my ($phonix_file, $infile, $outfile, @lines) = @_;

    print "Writing input lexical forms to $infile\n";
    open my $in, ">", $infile or return wrn "Couldn't open $infile for writing: $!";
    print $in "$_\n" foreach @lines;

    my $cmd = "phonix $phonix_file -i $infile -o $outfile";
    print "Running `$cmd`\n";
    (system $cmd) == 0 or return err "Error running phonix: $!";

    print "Reading output lexical forms from $outfile\n";
    open my $out, "<", $outfile or return wrn "Couldn't open $outfile for reading: $!";
    return <$out>;
}
