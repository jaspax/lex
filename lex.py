from sqlalchemy import Column, Integer, String, DateTime, Interval, Numeric, ForeignKey
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine

engine = create_engine('sqlite+pysqlite:///lex.db') # Note: relative to PWD
sessionmaker = sessionmaker(bind=engine)
Base = declarative_base()

#
# EXPLAINING THE DATA MODEL
#
# RootLexicon is the basis of everything; it has only:
#   id : the unique ID for the lexical item
#   tableRef : a column which identifies the subclass
#   head : the head entry (basic lexical item)
#
# All other columns (including "gloss", which exists in most but not all
# subclasses) are defined on sub-classes (which are sub-tables).
#
# Derivational relationships are defined via the Relation table, which allows us
# to create many-to-many relationships from RootLexicon to itself (a word can
# have multiple sources; multiple words may be derived from the same source).
# Multiple different kinds of Relations are defined with constants on the
# Relation class.
#

class Relation(Base):
    """A class which represents a derivational relationship of some kind"""
    __tablename__ = "Relation"

    item_id = Column(Integer, ForeignKey("RootLexicon.id"), nullable=False, primary_key=True)
    source_id = Column(Integer, ForeignKey("RootLexicon.id"), nullable=False, primary_key=True)
    type = Column(String)

    ## Constants used for the "type" column
    BORROWED = "Borrowed"       # Borrowed from another language language
    INHERITED = "Inherited"     # Inherited from its parent, modified by normal sound change
    DERIVED = "Derived"         # Derived from another word in the same language by synchronic derivational morphology
    COMPOUND = "Compound"       # Derived from word(s) in the same language by compounding

class RootLexicon(Base):
    """The root class from which all tables derive"""
    __tablename__ = "RootLexicon"

    id = Column(Integer, primary_key=True)
    tableRef = Column(String, nullable=False)
    head = Column(String, nullable=False)

    sources = relationship("Relation", 
            secondary = Relation.__table__,
            primaryjoin = id == Relation.item_id,
            secondaryjoin = id == Relation.source_id,
            backref = "item")

    __mapper_args__ = {
        'polymorphic_identity': __tablename__,
        'polymorphic_on': tableRef
    }

class CommonYivrian(RootLexicon):
    """CommonYivrian, the ancestor of all the Yivril languages"""
    __tablename__ = "CommonYivrian"

    id = Column(Integer, ForeignKey("RootLexicon.id"), primary_key=True)
    note = Column(String)

    __mapper_args__ = {
        'polymorphic_identity': __tablename__,
    }

class Praseo(RootLexicon):
    """Praseo, the language of the city of Prasa"""
    __tablename__ = "Praseo"

    id = Column(Integer, ForeignKey("RootLexicon.id"), primary_key=True)
    gloss = Column(String, nullable=False)
    partOfSpeech = Column(String)
    nounClass = Column(String)
    verb = Column(String)
    verbForm = Column(String)
    verbClass = Column(String)
    note = Column(String)

    __mapper_args__ = {
        'polymorphic_identity': __tablename__,
    }

class Yivrian(RootLexicon):
    """Praseo, the language of the city of Prasa"""
    __tablename__ = "Yivrian"

    id = Column(Integer, ForeignKey("RootLexicon.id"), primary_key=True)
    gloss = Column(String, nullable=False)
    partOfSpeech = Column(String, nullable=False)
    nounClass = Column(String)
    verb = Column(String)
    verbForm = Column(String)
    verbClass = Column(String)
    note = Column(String)

    __mapper_args__ = {
        'polymorphic_identity': __tablename__,
    }

class OldTzingrizhil(RootLexicon):
    """Old Tzingrizhil, the ancestor of Praseo and Tsingrizhil"""
    __tablename__ = "OldTzingrizhil"

    id = Column(Integer, ForeignKey("RootLexicon.id"), primary_key=True)

    __mapper_args__ = {
        'polymorphic_identity': __tablename__,
    }

class ProtoYivril(RootLexicon):
    """Proto-Yivril, the ancestor of everything"""
    __tablename__ = "ProtoYivril"

    id = Column(Integer, ForeignKey("RootLexicon.id"), primary_key=True)
    gloss = Column(String, nullable=False)
    note = Column(String)

    __mapper_args__ = {
        'polymorphic_identity': __tablename__,
    }

Base.metadata.create_all(engine)
