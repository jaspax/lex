#!/usr/bin/perl
use strict;
use warnings;

use Class::DBI::SQLite;

package Lex;
use base qw(Class::DBI::SQLite);
Lex->connection("dbi:SQLite:dbname=lex.db", "", "");

my %tables;
my %aliases;

# 
# static methods
#

sub table($$) {
    my ($class, $primary, @aliases) = @_;

    if ($primary) {
        # make the table name and all aliases keys in our global %tables hash
        $tables{$primary} = $class;
        $aliases{$_} = $primary foreach @aliases;

        # only pass the first element of @names to the super-class, because we
        # don't want to use Class::DBI::SQLite's table-aliasing 
        $class->SUPER::table($primary);
    }
    else {
        $class->SUPER::table();
    }
}

sub trigger_add_root_lexicon($$) {
    my ($table, $col) = @_;
    die "Table and column name required" unless $table and $col;
    return sub {
        my $self = shift;
        my $root = Lex::RootLexicon->insert({ tableRef => $table });
        $self->set( $col => $root->id );
    }
}

sub trigger_delete_root_lexicon($) {
    my $col = shift;
    die "Column name required" unless $col;
    return sub {
        my $self = shift;
        my $root = Lex::RootLexicon->retrieve($self->$col);
        $root->delete();
    }
}

sub tables {
    return keys %tables;
}

sub aliases {
    my $full_name = shift;

    # When given a full name, return the aliases pointing to that name
    if ($full_name) {
        return grep { $aliases{$_} eq $full_name } keys %aliases;
    }

    # Otherwise just return all aliases
    return keys %aliases;
}

sub table_class($) {
    my $class = shift;
    die "Can't get undefined table class" if not $class;

    my $rv = $tables{$class};
    if (not $rv) {
        $rv = $tables{$aliases{$class}} if exists $aliases{$class};
    }
        
    die "Undefined table class for $class" unless $rv;
    return $rv;
}

sub columns {
    my $self = shift;

    if (@_) {
        # in the multi-argument form, use the superclass
        return $self->SUPER::columns(@_);
    }

    my %used;
    return grep { my $rv = not $used{$_}; $used{$_} = 1; $rv } 
        $self->columns("Primary"), $self->columns("Essential"), sort $self->columns("All");
}

# get the entry in the RootLexicon
sub rootEntry {
    my $self = shift;
    return Lex::RootLexicon->retrieve($self->id);
}

package Lex::Scratch;
use base qw(Lex);
Lex::Scratch->table("Scratch");
Lex::Scratch->columns(All => qw/id num note/);

package Lex::RootLexicon;
use base qw(Lex);
Lex::RootLexicon->table("RootLexicon");
Lex::RootLexicon->columns(All => qw/id tableRef/);

sub entry() {
    my $self = shift;
    my $table = Lex::table_class($self->tableRef);
    return $table->retrieve($self->id);
}

# 
# Note the following conventions used on all language tables.
#
# Primary Key is named id, and references a matching id in RootLexicon
#
# Other important keys are marked Essential.
#
# There is a before_create and an after_delete trigger which maintains ref.
# integrity to the RootLexicon.
#
# There is a source column which refers to the etymological source in the
# RootLexicon.
#
# The lexicon head column for each table matches the table short name. Ie. the
# Yivrian head entry column is yiv, etc. (The "phonix" command depends on this
# convention.)
#

package Lex::Yivrian;
use base qw(Lex);
Lex::Yivrian->table("Yivrian", "yiv");
Lex::Yivrian->columns(Primary => qw/id/);
Lex::Yivrian->columns(Essential => qw/yiv gloss verb/);
Lex::Yivrian->columns(Others => qw/partOfSpeech nounClass verbForm verbClass source note/);
Lex::Yivrian->has_a(source => 'Lex::RootLexicon');
Lex::Yivrian->add_trigger(before_create => Lex::trigger_add_root_lexicon("Yivrian", "id"));
Lex::Yivrian->add_trigger(after_delete => Lex::trigger_delete_root_lexicon("id"));

package Lex::CommonYivrian;
use base qw(Lex);
Lex::CommonYivrian->table("CommonYivrian", "cy");
Lex::CommonYivrian->columns(Primary => qw/id/);
Lex::CommonYivrian->columns(Essential => qw/cy source/);
Lex::CommonYivrian->has_a(source => 'Lex::RootLexicon');
Lex::CommonYivrian->add_trigger(before_create => Lex::trigger_add_root_lexicon("CommonYivrian", "id"));
Lex::CommonYivrian->add_trigger(after_delete => Lex::trigger_delete_root_lexicon("id"));

package Lex::ProtoYivril;
use base qw(Lex);
Lex::ProtoYivril->table("ProtoYivril", "py");
Lex::ProtoYivril->columns(Primary => qw/id/);
Lex::ProtoYivril->columns(Essential => qw/py gloss note/);
Lex::ProtoYivril->add_trigger(before_create => Lex::trigger_add_root_lexicon("ProtoYivril", "id"));
Lex::ProtoYivril->add_trigger(after_delete => Lex::trigger_delete_root_lexicon("id"));

package Lex::OldTzingrizhil;
use base qw(Lex);
Lex::OldTzingrizhil->table("OldTzingrizhil", "otz");
Lex::OldTzingrizhil->columns(Primary => qw/id/);
Lex::OldTzingrizhil->columns(Essential => qw/otz source/);
Lex::OldTzingrizhil->has_a(source => 'Lex::RootLexicon');
Lex::OldTzingrizhil->add_trigger(before_create => Lex::trigger_add_root_lexicon("OldTzingrizhil", "id"));
Lex::OldTzingrizhil->add_trigger(after_delete => Lex::trigger_delete_root_lexicon("id"));

package Lex::Praseo;
use base qw(Lex);
Lex::Praseo->table("Praseo", "pr");
Lex::Praseo->columns(Primary => qw/id/);
Lex::Praseo->columns(Essential => qw/pr gloss verb source/);
Lex::Praseo->has_a(source => 'Lex::RootLexicon');
Lex::Praseo->add_trigger(before_create => Lex::trigger_add_root_lexicon("Praseo", "id"));
Lex::Praseo->add_trigger(after_delete => Lex::trigger_delete_root_lexicon("id"));

